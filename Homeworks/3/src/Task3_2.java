import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Task3_2 {
    /*
    Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

    Было:
    34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
    Стало после применения процедуры:
    34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
     */
    public static void main(String[] args) {

        System.out.print("Введите количество элементов: ");
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        List<Integer> list = new ArrayList<Integer>();
        System.out.println("Введите число:");
        for (int i = 0; i < count; i++) {
            System.out.print("Число с индексом " + Integer.toString(i) + ": ");
            list.add(sc.nextInt());
        }
        System.out.println("Перемещаем нули вправо:");
        for (int i = 0; i < count; i++) {
            if (list.get(i) == 0) {
                list.remove(i);
                list.add(0);
                i--;
                count--;
            }
        }
        System.out.println(Arrays.toString(list.toArray()));
    }
}

