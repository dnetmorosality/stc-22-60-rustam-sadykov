import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Task3_1 {
/*
Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа
в массиве. Если число в массиве отсутствует - вернуть -1.
 */

    public static void main(String[] args) {
        System.out.print("Введите количество элементов: ");
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        List<Integer> list = new ArrayList<Integer>();
        System.out.println("Введите число:");
        for (int i = 0; i < count; i++) {
            System.out.print("Число с индексом " + Integer.toString(i) + ": ");
            list.add(sc.nextInt());
        }
        System.out.println("Введите целое число для поиска в массиве:");
        int desiredNum = sc.nextInt();
        List<Integer> indexList = new ArrayList<Integer>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == desiredNum) {
                indexList.add(i);
            }
        }
        if (indexList.size() == 0) {
            System.out.println("-1");
        } else {
            System.out.println("Искомое число имеет индексы: " + Arrays.toString(indexList.toArray()));
        }
    }
}
