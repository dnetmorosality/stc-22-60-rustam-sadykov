public class Square extends Rectangle implements Moveable {

    public Square(int x, int y, int length) {
        super(x, y, length, length);
    }

    @Override
    public int getLength() {
        return super.getLength();
    }

    @Override
    public void setLength(int length) {
        super.setLength(length);
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    double perimeter() {
        return getLength() * 4;
    }


}
