public class Ellipse extends Figure {
    private int semiMinorAxis;
    private int semiMajorAxis;

    public Ellipse(int x, int y, int semiMinorAxis, int semiMajorAxis) {
        super(x, y);
        this.semiMinorAxis = semiMinorAxis;
        this.semiMajorAxis = semiMajorAxis;
    }

    public int getSemiMinorAxis() {
        return semiMinorAxis;
    }

    public void setSemiMinorAxis(int semiMinorAxis) {
        this.semiMinorAxis = semiMinorAxis;
    }

    public int getSemiMajorAxis() {
        return semiMajorAxis;
    }

    public void setSemiMajorAxis(int semiMajorAxis) {
        this.semiMajorAxis = semiMajorAxis;
    }

    @Override
    double perimeter() {
        return 4 * (Math.PI * semiMinorAxis * semiMajorAxis + Math.pow(semiMajorAxis - semiMinorAxis, 2)) / (semiMajorAxis + semiMinorAxis);
    }
}
