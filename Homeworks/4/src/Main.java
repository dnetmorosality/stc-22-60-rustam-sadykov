public class Main {
    public static void main(String[] args) {
        Figure[] figures = new Figure[4];

        figures[0] = new Rectangle(0, 0, 100, 50);
        figures[1] = new Square(0, 0, 100);
        figures[2] = new Ellipse(0, 0, 100, 50);
        figures[3] = new Circle(0, 0, 100);

        for (Figure figure : figures) {
            System.out.println(figure.getClass() + " perimeter equals: " + String.valueOf(figure.perimeter()));
        }
        System.out.println();
        Square square = (Square) figures[1];
        Circle circle = (Circle) figures[3];

        System.out.println("Сurrent coordinates square: " + square.toString());
        System.out.println("Сurrent coordinates circle: " + circle.toString());
        System.out.println();
        square.move((int) (Math.random() * 1000), (int) (Math.random() * 1000));
        circle.move((int) (Math.random() * 1000), (int) (Math.random() * 1000));

        System.out.println("New coordinates square: " + square.toString());
        System.out.println("New coordinates circle: " + circle.toString());


    }
}
