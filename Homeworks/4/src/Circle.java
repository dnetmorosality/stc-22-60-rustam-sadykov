public class Circle extends Ellipse implements Moveable {

    private int semidiameter;

    public Circle(int x, int y, int semidiameter) {
        super(x, y, semidiameter, semidiameter);
        this.semidiameter = semidiameter;
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }

    public int getSemidiameter() {
        return semidiameter;
    }

    public void setSemidiameter(int semidiameter) {
        this.semidiameter = semidiameter;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * semidiameter;
    }


}

